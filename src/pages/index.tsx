import React from 'react';
import styles from './index.less';
import { TestButton } from "@lxb/test";

export default () => {
  return (
    <div>
      <h1 className={styles.title}>
        <TestButton />
        Page index
      </h1>
    </div>
  );
}
